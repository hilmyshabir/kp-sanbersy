<?php

use App\Models\Car;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $cars = Car::all();
    return view('home', compact('cars'));
});

Route::get('registerpage', function () {
    return view('auth.register');
});

Auth::routes(['verify' => true]);

//Admin cars
Route::middleware('role:admin')->get('cars', [App\Http\Controllers\CarsController::class, 'index']);
Route::middleware('role:admin')->get('/cars/create', [App\Http\Controllers\CarsController::class, 'create']);
Route::middleware('role:admin')->post('/cars', [App\Http\Controllers\CarsController::class, 'store']);
Route::middleware('role:admin')->get('edit/{id}', [App\Http\Controllers\CarsController::class, 'edit']);
Route::middleware('role:admin')->post('/cars/{id}', [App\Http\Controllers\CarsController::class, 'update']);
Route::middleware('role:admin')->delete('/cars/{id}', [App\Http\Controllers\CarsController::class, 'destroy']);

//Admin rentals
Route::middleware('role:admin')->get('rental_submissions', [App\Http\Controllers\RentalSubmissionsController::class, 'index']);
Route::middleware('role:admin')->post('setuju/{id}', [App\Http\Controllers\RentalSubmissionsController::class, 'setuju']);
Route::middleware('role:admin')->post('tolak/{id}', [App\Http\Controllers\RentalSubmissionsController::class, 'tolak']);
Route::middleware('role:admin')->get('history', [App\Http\Controllers\RentalSubmissionsController::class, 'history']);

//Guest
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

//User profile
Route::get('profilepage', [App\Http\Controllers\ProfileController::class, 'index']);
Route::post('profile', [App\Http\Controllers\ProfileController::class, 'update']);

//User rentals
Route::get('rental/{id}', [App\Http\Controllers\RentalsController::class, 'index']);
Route::post('rental/{id}', [App\Http\Controllers\RentalsController::class, 'store']);
Route::get('submissions', [App\Http\Controllers\RentalsController::class, 'submissions']);






Route::middleware('role:admin')->get('/dashboard', function () {
    return view('admin.dashboard');
})->name('dashboard');

Route::middleware('role:admin')->get('/icons', function () {
    return view('admin.icons');
})->name('icons');

Route::middleware('role:admin')->get('/maps', function () {
    return view('admin.maps');
})->name('maps');

Route::middleware('role:admin')->get('/notifications', function () {
    return view('admin.notifications');
})->name('notifications');

Route::middleware('role:admin')->get('/user', function () {
    return view('admin.user');
})->name('user');

Route::middleware('role:admin')->get('/tables', function () {
    return view('admin.tables');
})->name('tables');

Route::middleware('role:admin')->get('/typography', function () {
    return view('admin.typography');
})->name('typography');

Route::middleware('role:admin')->get('/upgrade', function () {
    return view('admin.upgrade');
})->name('upgrade');
