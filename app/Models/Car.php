<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Car extends Model
{
    use HasFactory;

    protected $fillable = [
        'gambar',
        'nama_mobil',
        'kapasitas',
        'stok',
        'lokasi',
        'biaya_sewa'
    ];

    public function rental()
    {
        return $this->hasMany('App\Models\Rental', 'car_id', 'id');
    }
}
