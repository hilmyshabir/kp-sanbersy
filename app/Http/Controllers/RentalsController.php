<?php

namespace App\Http\Controllers;

use App\Models\Car;
use App\Models\Rental;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;

class RentalsController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $car = Car::where('id', $id)->first();
        return view('customer.rental', compact('car'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        $user = User::where('id', Auth::user()->id)->first();
        $car = Car::where('id', $id)->first();
        $tanggal = Carbon::now();

        //validasi profile user
        if (empty($user->nohp)) {
            Alert::toast('Mohon lengkapi data diri Anda', 'error');
            return redirect('profilepage');
        }
        if (empty($user->alamat)) {
            Alert::toast('Mohon lengkapi data diri Anda', 'error');
            return redirect('profilepage');
        }

        //validasi stok
        if ($car->stok < 1) {
            Alert::toast('Mobil tidak tersedia', 'error');
            return redirect('rental/' . $id);
        }

        //cek apakah mobil yang disewa sudah pernah disewa sebelumnya
        $cek_mobil = Rental::where('user_id', Auth::user()->id)->where('car_id', $car->id)->where('status', 0)->first();
        if (empty($cek_mobil)) {
            $rental = new Rental();
            $rental->user_id = Auth::user()->id;
            $rental->car_id = $car->id;
            $rental->tanggal = $tanggal;
            $rental->status = 0;
            $rental->jumlah_hari = $request->jumlah_hari;
            $rental->total_biaya = $request->jumlah_hari * $car->biaya_sewa;
            $rental->save();
        } else {
            $rental = Rental::where('user_id', Auth::user()->id)->where('car_id', $car->id)->where('status', 0)->first();
            $rental->jumlah_hari = $rental->jumlah_hari + $request->jumlah_hari;
            $rental->total_biaya = $rental->total_biaya + ($request->jumlah_hari * $car->biaya_sewa);
            $rental->update();
        }

        Alert::toast('Penyewaan mobil telah diajukan. Mohon ditunggu untuk informasi selanjutnya', 'info');
        return redirect('/home');
    }

    public function submissions()
    {
        $rentals = Rental::where('user_id', Auth::user()->id)->where('status', '!=', 0)->get();
        return view('customer.submission', compact('rentals'));
    }
}
