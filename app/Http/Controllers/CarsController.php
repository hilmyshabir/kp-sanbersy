<?php

namespace App\Http\Controllers;

use App\Models\Car;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;

class CarsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cars = Car::paginate(20);
        return view('admin.cars.index', compact('cars'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.cars.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $nm = $request->gambar;
        $namaGambar = $nm->getClientOriginalName();

        $mobil_baru = new Car();
        $mobil_baru->gambar = $namaGambar;
        $mobil_baru->nama_mobil = $request->nama_mobil;
        $mobil_baru->kapasitas = $request->kapasitas;
        $mobil_baru->stok = $request->stok;
        $mobil_baru->lokasi = $request->lokasi;
        $mobil_baru->biaya_sewa = $request->biaya_sewa;

        $nm->move(public_path() . '/assets_admin/img', $namaGambar);
        $mobil_baru->save();

        Alert::toast('Mobil baru telah ditambahkan', 'success');
        return redirect('cars');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $car = Car::where('id', $id)->first();
        return view('admin.cars.edit', compact('car'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ($request->gambar) {
            $nm = $request->gambar;
            $namaGambar = $nm->getClientOriginalName();

            $car = Car::where('id', $id)->first();
            $car->gambar = $namaGambar;
            $car->nama_mobil = $request->nama_mobil;
            $car->kapasitas = $request->kapasitas;
            $car->stok = $request->stok;
            $car->lokasi = $request->lokasi;
            $car->biaya_sewa = $request->biaya_sewa;
            $nm->move(public_path() . '/assets_admin/img', $namaGambar);
        } else {
            $car = Car::where('id', $id)->first();
            $car->nama_mobil = $request->nama_mobil;
            $car->kapasitas = $request->kapasitas;
            $car->stok = $request->stok;
            $car->lokasi = $request->lokasi;
            $car->biaya_sewa = $request->biaya_sewa;
        }

        $car->update();

        Alert::toast('Data mobil berhasil diedit', 'success');
        return redirect('cars');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $car = Car::where('id', $id)->first();
        $car->delete();

        Alert::toast('Data mobil telah dihapus', 'warning');
        return redirect('cars');
    }
}
