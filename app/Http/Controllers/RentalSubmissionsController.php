<?php

namespace App\Http\Controllers;

use App\Models\Car;
use App\Models\Rental;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;

class RentalSubmissionsController extends Controller
{
    //
    public function index()
    {
        // $rental = Rental::where('user_id', Auth::user()->id)->where('status', 0)->first();
        // $rental_details = [];
        // if (!empty($rental)) {
        //     $rental_details = RentalDetail::where('rental_id', $rental->id)->get();
        // }

        // return view('admin.rentals', compact('rental', 'rental_details'));

        $rentals = Rental::where('status', 0)->get();
        return view('admin.rentals', compact('rentals'));
    }

    public function setuju($id)
    {
        //update tabel Rental
        $rental = Rental::where('id', $id)->first();
        $rental->status = 1;
        $rental->update();

        //update tabel Car
        $car = Car::where('id', $rental->car_id)->first();
        $car->stok = $car->stok - 1;
        $car->update();

        Alert::toast('Menyetujui penyewaan mobil', 'success');
        return redirect('rental_submissions');
    }

    public function tolak($id)
    {
        $rental = Rental::where('id', $id)->where('status', 0)->first();

        $rental->status = 2;
        $rental->update();

        Alert::toast('Menolak penyewaan mobil', 'warning');
        return redirect('rental_submissions');
    }

    public function history()
    {
        $rentals = Rental::where('status', '!=', 0)->get();
        return view('admin.history', compact('rentals'));
    }
}
