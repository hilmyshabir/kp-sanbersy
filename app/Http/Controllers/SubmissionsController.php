<?php

namespace App\Http\Controllers;

use App\Models\Rental;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SubmissionsController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }

    public function index()
    {
        $rentals = Rental::where('user_id', Auth::user()->id)->where('status', '!=', 0)->get();

        return view('customer.submission', compact('rentals'));
    }
}
