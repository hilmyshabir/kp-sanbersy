# Sistem Informasi Penyewaan Mobil

Rental Mobil Citra akan merancang sebuah aplikasi berbasis website untuk melakukan pengelolaan data penyewaan mobil. Roles aplikasi ada 2, yaitu admin / pemilik rental dan customer. Fitur minimal yang diharapkan yaitu : 
1. Customer bisa melihat galeri mobil dan spesifikasi yang dimiliki tempat rental (tanpa login)
2. Customer harus memiliki akun untuk bisa login ke aplikasi dan mengajukan sewa mobil
3. Akun customer harus terverifikasi emailnya
4. Admin dapat mengelola data - data mobil yang dimiliki
5. Admin dapat menyetujui / menolak pengajuan penyewaan mobil dari customer
6. Admin dapat memantau riwayat peminjaman mobil 
