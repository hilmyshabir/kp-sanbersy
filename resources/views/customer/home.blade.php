@extends('layouts.customer-layout')

@section('content')
@if (session('status'))
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endif
<div class="page-header" data-parallax="true" style="background-image: url('../assets/img/garage-customer.jpg');">
    <div class="filter"></div>
    <div class="container">
        <div class="motto text-center">
            <h1>Sekut Rental</h1>
            <h3>Daripada beli mobil mending nyewa aja</h3>
            <br />
            <a href="{{ url('profilepage') }}" class="btn btn-outline-neutral btn-round"><i class="fa fa-play"></i>Profile Page</a>
            <a href="{{ url('registerpage') }}" class="btn btn-outline-neutral btn-round"><i class="fa fa-play"></i>Register Page</a>
        </div>
    </div>
</div>
<div class="main">
    <div class="section text-center">
        <div class="container">
            <div class="row">
                <div class="col-md-8 ml-auto mr-auto">
                    <h2 class="title">Apa itu Sekut Rental?</h2>
                    <h5 class="description">Sekut Rental merupakan website yang menyediakan layanan penyewaan mobil untuk harian maupun mingguan. Disini kami menyediakan berbagai jenis mobil premium yang dapat anda sewa sehingga anda tidak perlu menunggu lama untuk dapat merasakan kemewahan serta kesenangan dari mobil premium impian anda. Tunggu apa lagi? Segera daftar sekarang.</h5>
                    <br>
                    <a href="#paper-kit" class="btn btn-danger btn-round">See Details</a>
                </div>
            </div>
            <br />
            <br />
            <div class="row">
                <div class="col-md-3">
                    <div class="info">
                        <div class="icon icon-danger">
                            <i class="nc-icon nc-bus-front-12"></i>
                        </div>
                        <div class="description">
                            <h4 class="info-title">Mobil Premium</h4>
                            <p class="description">Tersedia berbagai mobil premium impian anda mulai dari sedan kencang 4 pintu hingga SUV.</p>
                            <a href="javascript:;" class="btn btn-link btn-danger">See more</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="info">
                        <div class="icon icon-danger">
                            <i class="nc-icon nc-bulb-63"></i>
                        </div>
                        <div class="description">
                            <h4 class="info-title">Easy to Use</h4>
                            <p>Semudah membalikkan tangan, anda hanya perlu kirimkan foto selfie dengan KTP anda.</p>
                            <a href="javascript:;" class="btn btn-link btn-danger">See more</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="info">
                        <div class="icon icon-danger">
                            <i class="nc-icon nc-chart-bar-32"></i>
                        </div>
                        <div class="description">
                            <h4 class="info-title">Terpercaya</h4>
                            <p>Sudah ada ratusan pelanggan kami yang selalu menyewa mobil-mobil kami.</p>
                            <a href="javascript:;" class="btn btn-link btn-danger">See more</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="info">
                        <div class="icon icon-danger">
                            <i class="nc-icon nc-sun-fog-29"></i>
                        </div>
                        <div class="description">
                            <h4 class="info-title">Pelayanan Mewah</h4>
                            <p>Anda akan mendapatkan pelayanan termewah yang tidak anda dapatkan di tempat lain.</p>
                            <a href="javascript:;" class="btn btn-link btn-danger">See more</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section section-dark text-center">
        <div class="container">
            <h2 class="title">Foto-foto Tempat Kami</h2>
            <div class="row">
                <div class="col-md-8 ml-auto mr-auto">
                    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                        <ol class="carousel-indicators">
                            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                        </ol>
                        <div class="carousel-inner" role="listbox" style=" width:100%; height: 450px !important;">
                            <div class="carousel-item active">
                                <img class="d-block w-100" src="{{ url('../assets/img/store-1.jpg') }}">
                            </div>
                            <div class="carousel-item">
                                <img class="d-block w-100" src="{{ url('../assets/img/store-2.jpg') }}">
                            </div>
                            <div class="carousel-item">
                                <img class="d-block w-100" src="{{ url('../assets/img/store-3.jpg') }}">
                            </div>
                        </div>
                        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section text-center">
        <div class="container">
            <h2 class="title">Pilih Mobil Impian Anda</h2>
            <div class="row">
                @foreach( $cars as $car )
                <div class="col-md-4">
                    <div class="card">
                        <img src="{{ url('assets_admin/img') }}/{{ $car->gambar }}" class="card-img-top">
                        <div class="card-body">
                            <h5 class="card-title">{{ $car->nama_mobil }}</h5>
                            <p class="card-text" align="left">
                                <strong>Kapasitas Penumpang : </strong>{{ $car->kapasitas }} orang<br>
                                <strong>Stok : </strong>{{ $car->stok }} <br>
                                <strong>Lokasi : </strong>{{ $car->lokasi }} <br>
                                <hr>
                                <strong>Biaya Sewa : </strong>Rp {{ number_format($car->biaya_sewa) }}
                            </p>
                            <a href="/rental/{{ $car->id }}" class="btn btn-warning btn-round">Sewa</a>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
    <div class="section section-dark text-center">
        <div class="container">
            <h2 class="title">Developer Sekut Rental</h2>
            <div class="row">
                <div class="col-md-4">
                    <div class="card card-profile card-plain">
                        <div class="card-avatar">
                            <a href="#avatar">
                                <img src="../assets/img/faces/GUA.jpg" alt="...">
                            </a>
                        </div>
                        <div class="card-body">
                            <a href="#paper-kit">
                                <div class="author">
                                    <h4 class="card-title">Hilmy Shabir</h4>
                                    <h6 class="card-category">Project Manager</h6>
                                </div>
                            </a>
                            <p class="card-description text-center">
                                Teamwork is so important that it is virtually impossible for you to reach the heights of your capabilities or make the money that you want without becoming very good at it.
                            </p>
                        </div>
                        <div class="card-footer text-center">
                            <a href="#pablo" class="btn btn-link btn-just-icon btn-neutral"><i class="fa fa-twitter"></i></a>
                            <a href="#pablo" class="btn btn-link btn-just-icon btn-neutral"><i class="fa fa-google-plus"></i></a>
                            <a href="#pablo" class="btn btn-link btn-just-icon btn-neutral"><i class="fa fa-linkedin"></i></a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card card-profile card-plain">
                        <div class="card-avatar">
                            <a href="#avatar">
                                <img src="../assets/img/faces/drake.jpg" alt="...">
                            </a>
                        </div>
                        <div class="card-body">
                            <a href="#paper-kit">
                                <div class="author">
                                    <h4 class="card-title">Abang Drake</h4>
                                    <h6 class="card-category">Designer</h6>
                                </div>
                            </a>
                            <p class="card-description text-center">
                                A group becomes a team when each member is sure enough of himself and his contribution to praise the skill of the others. No one can whistle a symphony. It takes an orchestra to play it.
                            </p>
                        </div>
                        <div class="card-footer text-center">
                            <a href="#pablo" class="btn btn-link btn-just-icon btn-neutral"><i class="fa fa-twitter"></i></a>
                            <a href="#pablo" class="btn btn-link btn-just-icon btn-neutral"><i class="fa fa-google-plus"></i></a>
                            <a href="#pablo" class="btn btn-link btn-just-icon btn-neutral"><i class="fa fa-linkedin"></i></a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card card-profile card-plain">
                        <div class="card-avatar">
                            <a href="#avatar">
                                <img src="../assets/img/faces/kaci-baum-2.jpg" alt="...">
                            </a>
                        </div>
                        <div class="card-body">
                            <a href="#paper-kit">
                                <div class="author">
                                    <h4 class="card-title">Lala Lili</h4>
                                    <h6 class="card-category">Developer</h6>
                                </div>
                            </a>
                            <p class="card-description text-center">
                                The strength of the team is each individual member. The strength of each member is the team. If you can laugh together, you can work together, silence isn’t golden, it’s deadly.
                            </p>
                        </div>
                        <div class="card-footer text-center">
                            <a href="#pablo" class="btn btn-link btn-just-icon btn-neutral"><i class="fa fa-twitter"></i></a>
                            <a href="#pablo" class="btn btn-link btn-just-icon btn-neutral"><i class="fa fa-google-plus"></i></a>
                            <a href="#pablo" class="btn btn-link btn-just-icon btn-neutral"><i class="fa fa-linkedin"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section landing-page">
        <div class="container">
            <div class="row">
                <div class="col-md-8 ml-auto mr-auto">
                    <h2 class="text-center">Ingin Menjadi Bagian dari Kami?</h2>
                    <form class="contact-form">
                        <div class="row">
                            <div class="col-md-6">
                                <label>Nama</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">
                                            <i class="nc-icon nc-single-02"></i>
                                        </span>
                                    </div>
                                    <input type="text" class="form-control" placeholder="Nama">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label>Email</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">
                                            <i class="nc-icon nc-email-85"></i>
                                        </span>
                                    </div>
                                    <input type="text" class="form-control" placeholder="Email">
                                </div>
                            </div>
                        </div>
                        <label>Alasan ingin bergabung</label>
                        <textarea class="form-control" rows="4" placeholder="Sebutkan alasan mengapa anda ingin bergabung dengan kami..."></textarea>
                        <div class="row">
                            <div class="col-md-4 ml-auto mr-auto">
                                <button class="btn btn-danger btn-lg btn-fill">Kirim Lamaran</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection