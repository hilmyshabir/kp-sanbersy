@extends('layouts.customer-layout')

@section('content')
<div class="page-header page-header-xs" data-parallax="true" style="background-image: url('../assets/img/garage-profile.jpg');">
    <div class="filter"></div>
</div>
<div class="section profile-content">
    <div class="container">
        <div class="owner">
            <div class="avatar">
                @if( !empty($user->gambar) )
                <img src="{{ url('assets/img/faces') }}/{{ $user->gambar }}" alt="Circle Image" class="img-circle img-no-padding img-responsive">
                @else
                <img src="../assets/img/faces/default-avatar.png" alt="Circle Image" class="img-circle img-no-padding img-responsive">
                @endif
            </div>
            <div class="name">
                <h4 class="title" style="text-transform:uppercase">{{ $user->name }}
                    <br />
                </h4>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 ml-auto mr-auto text-center">
                <form action="{{ url('profile') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <table class="table table-borderless">
                        <tbody>
                            <tr>
                                <td class="text-right">Upload Profile Picture</td>
                                <td>:</td>
                                <td>
                                    <input id="gambar" type="file" class="form-control @error('gambar') is-invalid @enderror" name="gambar" value="{{ $user->gambar }}">

                                    @error('gambar')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </td>
                            </tr>

                            <tr>
                                <td class="text-right">Nama</td>
                                <td>:</td>
                                <td>
                                    <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ $user->name }}" required autocomplete="name" autofocus>

                                    @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </td>
                            </tr>

                            <tr>
                                <td class="text-right">Email</td>
                                <td>:</td>
                                <td>
                                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $user->email }}" required autocomplete="email">

                                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </td>
                            </tr>

                            <tr>
                                <td class="text-right">No. HP</td>
                                <td>:</td>
                                <td>
                                    <input id="nohp" type="nohp" class="form-control @error('nohp') is-invalid @enderror" name="nohp" value="{{ $user->nohp }}" required autocomplete="nohp">

                                    @error('nohp')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </td>
                            </tr>

                            <tr>
                                <td class="text-right">Alamat</td>
                                <td>:</td>
                                <td>
                                    <textarea name="alamat" class="form-control @error('alamat') is-invalid @enderror" required="">{{ $user->alamat }}</textarea>

                                    @error('alamat')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </td>
                            </tr>

                            <tr>
                                <td class="text-right">New Password</td>
                                <td>:</td>
                                <td>
                                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password">

                                    @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </td>
                            </tr>

                            <tr>
                                <td class="text-right">Confirm Password</td>
                                <td>:</td>
                                <td>
                                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation">
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <button type="submit" class="btn btn-outline-default btn-round">
                        <i class="fa fa-cog"></i>
                        Edit Profile
                    </button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection