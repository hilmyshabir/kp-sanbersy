@extends('layouts.customer-layout')

@section('content')
<div class="page-header page-header-xs" data-parallax="true" style="background-image: url('../assets/img/garage-customer.jpg');">
    <div class="filter"></div>
</div>
<div class="main">
    <div class="section">
        <div class="row">
            <div class="col-md-12">
                <a href="{{ url('home') }}" class="btn btn-primary btn-round">Kembali</a>
            </div>
            <div class="col-md-12 mt-2">
                <nav aria-label="breadcrumb" role="navigation">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ url('home') }}">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Pengajuan Sewa Mobil</li>
                    </ol>
                </nav>
            </div>
            <div class="col-md-12">
                @foreach( $rentals as $rental )
                <div class="card mt-5">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6">
                                <img src="{{ url('assets_admin/img') }}/{{ $rental->car->gambar }}" class="rounded mx-auto d-block" width="100%">
                            </div>
                            <div class="col-md-6">
                                <h2>{{ $rental->car->nama_mobil }}</h2>
                                <table class="table mt-5">
                                    <tbody>
                                        <tr>
                                            <td>Kapasitas</td>
                                            <td>:</td>
                                            <td>{{ $rental->car->kapasitas }} orang</td>
                                        </tr>
                                        <tr>
                                            <td>Lokasi</td>
                                            <td>:</td>
                                            <td>{{ $rental->car->lokasi }}</td>
                                        </tr>
                                        <tr>
                                            <td>Lama Sewa</td>
                                            <td>:</td>
                                            <td>{{ $rental->jumlah_hari }} hari</td>
                                        </tr>
                                        <tr>
                                            <td><strong>Total Biaya Sewa</strong></td>
                                            <td><strong>:</strong></td>
                                            <td><strong>Rp {{ number_format($rental->total_biaya) }}</strong></td>
                                        </tr>
                                        <tr>
                                            @if( $rental->status == 1 )
                                            <td>Status</td>
                                            <td>:</td>
                                            <td><strong class="text-success">DITERIMA</strong></td>
                                            @else
                                            <td>Status</td>
                                            <td>:</td>
                                            <td><strong class="text-danger">DITOLAK</strong></td>
                                            @endif
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
</div>
@endsection