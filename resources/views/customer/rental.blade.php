@extends('layouts.customer-layout')

@section('content')
<div class="page-header page-header-xs" data-parallax="true" style="background-image: url('../assets/img/garage-customer.jpg');">
    <div class="filter"></div>
</div>
<div class="main">
    <div class="section">
        <div class="row">
            <div class="col-md-12">
                <a href="{{ url('home') }}" class="btn btn-primary btn-round">Kembali</a>
            </div>
            <div class="col-md-12 mt-2">
                <nav aria-label="breadcrumb" role="navigation">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ url('home') }}">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Sewa {{ $car->nama_mobil }}</li>
                    </ol>
                </nav>
            </div>
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6">
                                <img src="{{ url('assets_admin/img') }}/{{ $car->gambar }}" class="rounded mx-auto d-block" width="100%">
                            </div>
                            <div class="col-md-6">
                                <h2>{{ $car->nama_mobil }}</h2>
                                <table class="table mt-5">
                                    <tbody>
                                        <tr>
                                            <td>Kapasitas</td>
                                            <td>:</td>
                                            <td>{{ $car->kapasitas }} orang</td>
                                        </tr>
                                        <tr>
                                            <td>Stok</td>
                                            <td>:</td>
                                            <td>{{ $car->stok }}</td>
                                        </tr>
                                        <tr>
                                            <td>Lokasi</td>
                                            <td>:</td>
                                            <td>{{ $car->lokasi }}</td>
                                        </tr>
                                        <tr>
                                            <td><strong>Biaya Sewa</strong></td>
                                            <td><strong>:</strong></td>
                                            <td><strong>Rp {{ number_format($car->biaya_sewa) }}</strong></td>
                                        </tr>
                                        <tr>
                                            <td><strong>Sewa</strong></td>
                                            <td><strong>:</strong></td>
                                            <td>
                                                <form action="{{ url('rental') }}/{{ $car->id }}" method="POST">
                                                    @csrf
                                                    <input type="text" name="jumlah_hari" class="form-control" required="" placeholder="">
                                                    <button type="submit" class="btn btn-warning btn-round mt-2">Sewa</button>
                                                </form>
                                            </td>
                                            <td><strong>Hari</strong></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection