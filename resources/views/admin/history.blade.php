@extends('template')

@section('content')
<div class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Riwayat Pengajuan Sewa Mobil</h4>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table">
                            <thead class="text-primary">
                                <th>No.</th>
                                <th>Nama Penyewa</th>
                                <th>Mobil</th>
                                <th>Lama Sewa</th>
                                <th>Total Biaya</th>
                                <th>Status</th>
                            </thead>
                            <tbody>
                                @foreach( $rentals as $rental )
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $rental->user->name }}</td>
                                    <td>{{ $rental->car->nama_mobil }}</td>
                                    <td>{{ $rental->jumlah_hari }}</td>
                                    <td>{{ number_format($rental->total_biaya) }}</td>
                                    @if( $rental->status == 1 )
                                    <td><strong class="text-success">DITERIMA</strong></td>
                                    @else
                                    <td><strong class="text-danger">DITOLAK</strong></td>
                                    @endif
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection