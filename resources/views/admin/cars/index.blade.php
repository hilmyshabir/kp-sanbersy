@extends('template')

@section('content')
<div class="content">
    <div class="row">
        <div class="col-md-12">
            <a href="/cars/create" class="btn btn-success btn-round"><i class="nc-icon nc-simple-add"></i> Tambah Data Mobil</a>
        </div>
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Daftar Mobil</h4>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table">
                            <thead class=" text-primary">
                                <th>No.</th>
                                <th>Gambar</th>
                                <th>Mobil</th>
                                <th>Kapasitas</th>
                                <th>Stok</th>
                                <th>Lokasi</th>
                                <th>Biaya Sewa</th>
                                <th class="text-center">Aksi</th>
                            </thead>
                            <tbody>
                                @foreach( $cars as $car )
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>
                                        <img src="{{ url('assets_admin/img') }}/{{ $car->gambar }}" width="100" height="50">
                                    </td>
                                    <td>{{ $car->nama_mobil }}</td>
                                    <td>{{ $car->kapasitas }} orang</td>
                                    <td>{{ $car->stok }}</td>
                                    <td>{{ $car->lokasi }}</td>
                                    <td>Rp {{ number_format($car->biaya_sewa) }}</td>
                                    <td class="text-center">
                                        <a href="/edit/{{ $car->id }}" class="btn btn-primary btn-round btn-sm">Edit</a>
                                        <form action="{{ url('cars') }}/{{ $car->id }}" method="POST">
                                            @csrf
                                            {{ method_field('DELETE') }}
                                            <button type="submit" class="btn btn-danger btn-round btn-sm" onclick="return confirm('Apakah anda yakin untuk menghapus data ini?')">Hapus</button>
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection