@extends('template')

@section('content')
<div class="content">
    <div class="row">
        <div class="col-md-12">
            <a href="{{ url('cars') }}" class="btn btn-primary btn-round">Kembali</a>
        </div>
        <div class="col-md-12 mt-2">
            <nav aria-label="breadcrumb" role="navigation">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ url('cars') }}">Daftar Mobil</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Tambah Data Mobil</li>
                </ol>
            </nav>
        </div>
        <div class="col-md-12 mt-2">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Tambah Data Mobil</h4>
                </div>
                <div class="card-body">
                    <form action="/cars" method="POST" enctype="multipart/form-data">
                        @csrf
                        <label class="form-label" for="gambar"><strong>Upload Gambar</strong></label>
                        <input type="file" class="form-control" id="gambar" name="gambar">
                        <div class="form-group">
                            <label for="nama_mobil"><strong>Nama Mobil</strong></label>
                            <input type="text" class="form-control @error('nama_mobil') is-invalid @enderror" id="nama_mobil" placeholder="Masukkan nama mobil" name="nama_mobil" value="{{ old('nama_mobil') }}" required autocomplete="nama_mobil">
                            @error('nama_mobil')<div class="invalid-feedback">{{ $message }}</div>@enderror
                        </div>
                        <div class="form-group">
                            <label for="kapasitas"><strong>Kapasitas</strong></label>
                            <input type="text" class="form-control @error('kapasitas') is-invalid @enderror" id="kapasitas" placeholder="Kapasitas penumpang" name="kapasitas" value="{{ old('kapasitas') }}" required autocomplete="kapasitas">
                            @error('kapasitas')<div class="invalid-feedback">{{ $message }}</div>@enderror
                        </div>
                        <div class="form-group">
                            <label for="stok"><strong>Stok</strong></label>
                            <input type="text" class="form-control @error('stok') is-invalid @enderror" id="stok" placeholder="Masukkan stok" name="stok" value="{{ old('stok') }}" required autocomplete="stok">
                            @error('stok')<div class="invalid-feedback">{{ $message }}</div>@enderror
                        </div>
                        <div class="form-group">
                            <label for="lokasi"><strong>Lokasi</strong></label>
                            <input type="text" class="form-control @error('lokasi') is-invalid @enderror" id="lokasi" placeholder="Masukkan lokasi" name="lokasi" value="{{ old('lokasi') }}" required autocomplete="lokasi">
                            @error('lokasi')<div class="invalid-feedback">{{ $message }}</div>@enderror
                        </div>
                        <div class="form-group">
                            <label for="biaya_sewa"><strong>Biaya Sewa</strong></label>
                            <input type="text" class="form-control @error('biaya_sewa') is-invalid @enderror" id="biaya_sewa" placeholder="Masukkan biaya sewa" name="biaya_sewa" value="{{ old('biaya_sewa') }}" required autocomplete="biaya_sewa">
                            @error('biaya_sewa')<div class="invalid-feedback">{{ $message }}</div>@enderror
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-success">Simpan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection