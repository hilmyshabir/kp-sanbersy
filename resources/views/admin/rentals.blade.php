@extends('template')

@section('content')
<div class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Daftar Pengajuan Sewa Mobil</h4>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table">
                            <thead class="text-primary">
                                <th>No.</th>
                                <th>Nama Penyewa</th>
                                <th>Mobil</th>
                                <th>Lama Sewa</th>
                                <th>Total Biaya</th>
                                <th class="text-center">Aksi</th>
                            </thead>
                            <tbody>
                                @foreach( $rentals as $rental )
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $rental->user->name }}</td>
                                    <td>{{ $rental->car->nama_mobil }}</td>
                                    <td>{{ $rental->jumlah_hari }}</td>
                                    <td>{{ number_format($rental->total_biaya) }}</td>
                                    <td class="text-center">
                                        <form action="{{ url('setuju') }}/{{ $rental->id }}" method="POST">
                                            @csrf
                                            <button type="submit" class="btn btn-success btn-round btn-sm">Setuju</button>
                                        </form>
                                        <form action="{{ url('tolak') }}/{{ $rental->id }}" method="POST">
                                            @csrf
                                            <button type="submit" class="btn btn-danger btn-round btn-sm">Tolak</button>
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection