@extends('landingpage')

@section('content')
<div class="page-header" style="background-image: url('../assets/img/garage-login.jpg');">
    <div class="filter"></div>
    <div class="container">
        <div class="row">
            <div class="col-lg-4 ml-auto mr-auto">
                <div class="card card-register">
                    <h3 class="title mx-auto">Welcome</h3>
                    <div class="social-line text-center">
                        <a href="#pablo" class="btn btn-neutral btn-facebook btn-just-icon">
                            <i class="fa fa-facebook-square"></i>
                        </a>
                        <a href="#pablo" class="btn btn-neutral btn-google btn-just-icon">
                            <i class="fa fa-google-plus"></i>
                        </a>
                        <a href="#pablo" class="btn btn-neutral btn-twitter btn-just-icon">
                            <i class="fa fa-twitter"></i>
                        </a>
                    </div>
                    <form class="register-form" method="POST" action="{{ route('login') }}">
                        @csrf
                        <label>Email Address</label>
                        <input id="email" type="email" placeholder="Email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">
                        @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror

                        <label>Password</label>
                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
                        @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror

                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                            <label class="form-check-label" for="remember">
                                {{ __('Remember Me') }}
                            </label>
                        </div>

                        <button class="btn btn-danger btn-block btn-round" type="submit">
                            {{ __('Login') }}
                        </button>

                        @if (Route::has('password.request'))
                        <div class="forgot">
                            <a href="{{ route('password.request') }}" class="btn btn-link btn-danger">Forgot password?</a>
                        </div>
                        @endif
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection