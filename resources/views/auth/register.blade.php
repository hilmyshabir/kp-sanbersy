@extends('landingpage')

@section('content')
<div class="page-header" style="background-image: url('../assets/img/garage-login.jpg');">
    <div class="filter"></div>
    <div class="container">
        <div class="row">
            <div class="col-lg-4 ml-auto mr-auto">
                <div class="card card-register">
                    <h3 class="title mx-auto">Please register your account</h3>
                    <div class="social-line text-center">
                        <a href="#pablo" class="btn btn-neutral btn-facebook btn-just-icon">
                            <i class="fa fa-facebook-square"></i>
                        </a>
                        <a href="#pablo" class="btn btn-neutral btn-google btn-just-icon">
                            <i class="fa fa-google-plus"></i>
                        </a>
                        <a href="#pablo" class="btn btn-neutral btn-twitter btn-just-icon">
                            <i class="fa fa-twitter"></i>
                        </a>
                    </div>
                    <form class="register-form" method="POST" action="{{ route('register') }}">
                        @csrf
                        <label>Name</label>
                        <input id="name" type="text" placeholder="Name" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                        @error('name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror

                        <label>Email Address</label>
                        <input id="email" type="email" placeholder="Email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">
                        @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror

                        <label>Your address</label>
                        <input id="alamat" type="alamat" placeholder="Your address" class="form-control @error('alamat') is-invalid @enderror" name="alamat" value="{{ old('alamat') }}">
                        @error('alamat')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror

                        <label>Phone number</label>
                        <input id="nohp" type="nohp" placeholder="Phone number" class="form-control @error('nohp') is-invalid @enderror" name="nohp" value="{{ old('nohp') }}">
                        @error('nohp')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror

                        <label>Password</label>
                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
                        @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror

                        <label>Confirm Password</label>
                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">

                        <button class="btn btn-danger btn-block btn-round" type="submit">
                            {{ __('Register') }}
                        </button>
                    </form>
                    <div class="forgot">
                        <a href="#" class="btn btn-link btn-danger">Forgot password?</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection